<div id="delete" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <a href="javascript:void(0)" class="close" data-dismiss="modal"></a>
            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
            </div>
            <form id="form-delete" action="" class="form" method="post">
                @csrf
                @method('DELETE')
                <div class="modal-body">
                    <label>{{ $message }}
                        <span id="message" class="bold"></span>
                    </label>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="button dark"> Confirmar</button>
                </div>
            </form>
        </div>
    </div>
</div>