<div class="modal fade modal-dialog-center" id="cropper_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrap">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Editor de Imágenes</h4>
                </div>
                <div class="modal-body">
                    <div class="wrapper-cropper">
                        <img src="" id="image">
                    </div>
                </div>
                <div class="modal-footer">
                    {{--<button data-dismiss="modal" class="btn btn-red modal-btn-cancel" type="button">Cerrar</button>--}}
                    <button class="btn btn-blue modal-btn-confirm" type="button">Guardar cambios</button>
                </div>
            </div>
        </div>
    </div>
</div>
