<!--sidebar start-->
<aside>
    <div id="sidebar" class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu" id="nav-accordion">
            <li class="nav-only {{ Route::is('admin.home.index') ? 'active' : ''}}">
                <a class="" href="{{ route('admin.home.index') }}">Inicio</a>
            </li>
            <li class="sub-menu">
                <a href="javascript:void(0)" class="{{ Route::is('admin.home.job') ? 'active' : ''}}">
                    <span>Portafolio</span>
                </a>
                <ul class="sub">
                    <li class="{{ Route::is('admin.category.index') ? 'active' : ''}}">
                        <a href="{{ route('admin.category.index') }}">Categorías</a>
                    </li>
                    <li class="{{ Route::is('admin.project.index') ? 'active' : ''}}">
                        <a href="{{ route('admin.project.index') }}">Trabajos</a>
                    </li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:void(0)" class="">
                    <span>Nosotros</span>
                </a>
                <ul class="sub">
                    <li class="{{ Route::is('admin.agency.index') ? 'active' : ''}}">
                        <a href="{{ route('admin.agency.index') }}">La Agencia</a>
                    </li>
                    <li class="{{ Route::is('admin.client.index') ? 'active' : ''}}">
                        <a href="{{ route('admin.client.index') }}">Clientes</a>
                    </li>
                </ul>
            </li>
            <li class="nav-only {{ Route::is('admin.contact.*') ? 'active': '' }}">
                <a href="{{ route('admin.contact.index') }}">Contacto</a>
            </li>
            <li class="nav-only {{ Route::is('admin.message.*') ? 'active': '' }}">
                <a href="{{ route('admin.message.index') }}">Suscriptores</a>
            </li>
        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->
