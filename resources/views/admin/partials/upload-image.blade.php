<div class="fileupload fileupload-new d-flex" data-provides="fileupload">

    <div class="row">
        @isset($path)
        <div class="fileupload-new thumbnail btn-file before-show col-12"
             style="background-color: #EFEFEF">

                <img class="img-fluid" src="{{ $path }}"/>
            {{--@else
                <img src="{{ asset('img/default.png') }}" alt="" style="width: 250px"/>
            @endif--}}
            <input type="file" class="default" data-input="{{ $edit }}"/>
        </div>
        @endif
        <div class="fileupload-preview fileupload-exists thumbnail {{ $edit }} col-12"
             style="width: {{ $width }}px; height: {{ $height }}px; max-width: {{ $width }}px; max-height: {{ $height }}px; line-height: 20px;">
        </div>
        <div class="col-12">
        <span class="btn btn-white btn-file p-0">
            <span class="fileupload-new button" style="width: 100%">Seleccionar imagen</span>
            <span class="fileupload-exists button">Cambiar imagen</span>
            <input type="file" class="default"/>
        </span>
            <div class="d-flex mt-1">
                <a href="#" class="btn btn-danger fileupload-exists m-0" data-dismiss="fileupload">
                    <i class="fa fa-trash"></i> Eliminar
                </a>
                <button id="{{ $edit }}" type="button" class="btn btn-warning fileupload-exists mt-0 ml-2"
                        data-toggle="modal" data-input="{{ $edit }}" data-target="#cropper_modal">
                    <i class="fa fa-edit"></i>Editar
                </button>
            </div>
        </div>
    </div>

</div>
