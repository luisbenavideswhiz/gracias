<!--header start-->
<header class="header">
    <div class="brand">
        <!--logo start-->
        <a href="javascript:void(0)" class="logo">
            <img src="{{ url('img/logo.png') }}" alt="" width="160px" height="45px">
        </a>
        <!--logo end-->
    </div>
    <div class="top-nav">
        <a href="{{ route('logout') }}" class="button"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
            Cerrar sesión
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</header>
<!--header end-->