@extends('admin.layouts.base')
@section('bodyId')id="gallery" @endsection
@section('content')
<section class="wrapper">
    <!-- page start-->
    <div class="row">
        <div class="col-12">
            <h3 class="title d-flex justify-content-between align-items-center">
                Gallería - Cliente: {{ $project->client }}
            </h3>
        </div>
    </div>
    {!! Form::open(['route' => ['admin.gallery.store', $project->id], 'files' => true, 'method' => 'POST']) !!}
    <div class="row">
        <div class="col-9">
            <div class="card">
                <header class="card-header d-flex flex-row">
                    Galería de fotos (dimensiones 1170 x 658 px)
                </header>
                <div class="card-body">
                    <div class="ml-0">
                    <label class="col-md-4 p-0 control-label" for="radios">Seleccione tipo de contenido</label>
                    <div class="col-md-4 p-0">
                        <label class="radio-inline" for="radio-image">
                            <input type="radio" name="type" id="radio-image" value="image" checked="checked">
                            Imagen
                        </label>
                        <label class="radio-inline" for="radio-video">
                            <input type="radio" name="type" id="radio-video" value="video">
                            Video
                        </label>
                    </div>
                    </div>
                    <div id="div-images">
                        @component('admin.partials.upload-image', ['width' => 570, 'height' => 300])
                            @slot('edit') gallery @endslot
                        @endcomponent
                    </div>
                    <div id="div-videos" style="display: none">
                        <div class="col-12 p-0">
                            <div class="form-group">
                                {!! Field::text('video', null, ['label' => '', 'placeholder' => 'https://www.youtube.com/watch?v=xTI8U8KBGQo']) !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <br>
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered" id="table-gallery">
                        <tbody>
                        @foreach($galleries as $gallery)
                        <tr id="{{ $gallery->id }}">
                            <td class="text-center index">{{ $loop->iteration }}</td>
                            <td class="text-center">
                                @if($gallery->type == \App\Gallery::IMAGE)
                                    <img src="{{ $gallery->getPathImage(100, 56) }}" width="100px">
                                @else
                                    <img src="{{ asset('img/youtube-icon.png') }}" width="50px" >
                                @endif
                            </td>
                            <td><a href="{{ $gallery->getPathImage(1170, 658) }}" target="_blank">{{ $gallery->getPathImage(1170, 658) }}</a></td>
                            <td class="text-center">
                                <a href="javascript:void(0)" class="icon-delete delete-image" data-target="#delete" data-toggle="modal"
                                   data-action="{{ route('admin.gallery.destroy', $gallery->id) }}"><i class="material-icons">delete</i></a>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-3">
            <div class="action d-flex flex-row">
                <button type="submit" class="button dark active">Guardar</button>
                <a href="{{ route('admin.project.index') }}" class="button red ml-2">Cancelar</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
</section>
@include('admin.modals.loader')
@include('admin.modals.cropper-image')
@include('admin.modals.delete', [
   'title' => 'Eliminar Imagen',
   'message' => 'Esta seguro que desea eliminar esta imagen',
])
@endsection
@push('scripts')
    <script>
        $('.delete-image').on('click', function () {
            $('#form-delete').attr('action', $(this).data('action'));
            $('#delete').modal('show')
        });

        $('input[type=radio][name=type]').change(function() {
            if (this.value === 'image') {
                $('#div-images').show();
                $('#div-videos').hide();
            }
            else if (this.value === 'video') {
                $('#div-images').hide();
                $('#div-videos').show();
            }
        });
    </script>
@endpush