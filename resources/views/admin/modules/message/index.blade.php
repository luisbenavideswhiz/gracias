@extends('admin.layouts.base')
@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-12">
                <h3 class="title d-flex justify-content-between align-items-center">
                    Mensaje - Listado
                </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="table-projects" class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center w60">Orden</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Celular</th>
                                <th>Email</th>
                                <th class="text-center w120">Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($messages as $message)
                                <tr data-message="{{ $message->message }}">
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>{{ $message->first_name }}</td>
                                    <td>{{ $message->last_name }}</td>
                                    <td>{{ $message->phone }}</td>
                                    <td>{{ $message->email }}</td>
                                    <td class="text-center d-flex justify-content-around">
                                        <a href="javascript:void(0)" class="show" data-toggle="modal"
                                           data-target="#modal-show"><i class="material-icons">visibility</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.modules.message.modal.show')
@endsection
@push('scripts')
    <script>
        $('#table-projects').DataTable({
            language: {'url': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'},
            searching: false,
        });
        $('.show').on('click', function () {
            console.log($(this).parent().parent().data('message'));
            $('#message').val($(this).parent().parent().data('message'));
            $('#modal-show').modal();
        })
    </script>

@endpush