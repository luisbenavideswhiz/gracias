<div id="modal-show" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <a href="javascript:void(0)" class="close" data-dismiss="modal"></a>
            <div class="modal-header">
                <h5>Mensaje</h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <textarea rows="8" id="message" class="form-control" disabled></textarea>
                </div>
            </div>
        </div>
    </div>
</div>