@extends('admin.layouts.base')
@section('bodyId')id="category" @endsection
@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-12">
                <h3 class="title d-flex justify-content-between align-items-center">
                    Portafolio - Categorías
                    <button type="button" class="button dark" data-toggle="modal" data-target="#create">
                        Agregar nuevo
                    </button>
                </h3>
            </div>
        </div>
        @include('admin.partials.alerts')
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center w60">Orden</th>
                                <th>Nombre de categía</th>
                                <th>Trabajos asignados</th>
                                <th>Principal</th>
                                <th>F. de creación</th>
                                <th class="text-center w120">Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr data-name="{{ $category->name }}" data-id="{{ $category->id }}" data-checked="{{ $category->featured }}">
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>{{ $category->projects->count() }}</td>
                                    <td>{{ $category->featured ? 'Si' : 'No' }}</td>
                                    <td>{{ $category->created_at }}</td>
                                    <td class="text-center d-flex justify-content-around">
                                        <a href="javascript:void(0)" class="icon-edit edit-category"><i class="material-icons">edit</i></a>
                                        <a href="javascript:void(0)" class="icon-delete delete-category" data-toggle="modal" data-action="{{ route('admin.category.destroy', $category->id) }}"
                                           data-target="#delete"><i class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.modules.category.modal.create')
    @include('admin.modules.category.modal.edit')
    @include('admin.modals.loader')
    @include('admin.modals.delete', [
        'title' => 'Eliminar Categoría',
        'message' => 'Esta seguro que desea eliminar esta categoria',
    ])
@endsection
@push('scripts')
    <script>
        $('.delete-category').on('click', function () {
            $('#form-delete').attr('action', $(this).data('action'));
            $('#delete').modal('show')
        });
        $('.edit-category').on('click', function () {
            console.log($(this).parent().parent().data('name'));
            $('#category_name').val($(this).parent().parent().data('name'));
            $('#category_id').val($(this).parent().parent().data('id'));
            $('input[name=featured]').prop('checked', $(this).parent().parent().data('checked'));
            $('#modal-edit').modal();
        })
    </script>
@endpush