<div id="modal-edit" class="modal fade" role="dialog">
    <div class="modal-dialog modal-dialog-centered">

        <!-- Modal content-->
        <div class="modal-content">
            <a href="javascript:void(0)" class="close" data-dismiss="modal"></a>
            <div class="modal-header">
                <h5>Editar nueva categoría</h5>
            </div>
            {!! Form::open(['route' => 'admin.category.store']) !!}
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Nombre</label>
                    <input type="text" class="form-control" id="category_name" name="name">
                    <input type="hidden" class="form-control" id="category_id" name="id">
                </div>
                <div class="form-check">
                    <input id="featured" type="checkbox" class="form-check-input" name="featured" value="1">
                    <label class="form-check-label" for="featured">Principal</label>
                </div>
            </div>
            <div class="modal-footer">
                <div class="actions text-right">
                    <button type="submit" class="button dark">Guardar</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>