@extends('admin.layouts.base')
@section('bodyId')id="setting" @endsection
@section('content')
    <section class="wrapper">
        <div class="row">
            <div class="col-12">
                <h3 class="title d-flex justify-content-between align-items-center">
                    Nosotros - La agencia
                </h3>
            </div>
        </div>
        {!! Form::open(['route' => 'admin.setting.store']) !!}
        <div class="row">
            {!! Field::hidden('section', \App\Setting::AGENCY) !!}
            <div class="col-9">
                <div class="row">
                    <div class="col-6">
                        <div class="card">
                            <header class="card-header">
                                Nuestra Historia (Sección izquierda)
                            </header>
                            <div class="card-body">
                                <textarea class="summernote col-md-6" name="history_left">{{ $setting->data['history_left'] ?? null }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="card">
                            <header class="card-header">
                                Nuestra Historia (Sección derecha)
                            </header>
                            <div class="card-body">
                                <textarea class="summernote col-md-6" name="history_right">{{ $setting->data['history_right'] ?? null }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <header class="card-header">
                        Misión
                    </header>
                    <div class="card-body">
                        <textarea class="summernote" name="mission">{{ $setting->data['mission'] ?? null }}</textarea>
                    </div>
                </div>
                <br>
                <div class="card">
                    <header class="card-header">
                        Visión
                    </header>
                    <div class="card-body">
                        <textarea class="summernote" name="vision">{{ $setting->data['vision'] ?? null }}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="action d-flex justify-content-between">
                    <button type="submit" class="button dark active">Guardar</button>
                </div>
                <br>
                <div class="card">
                    <header class="card-header">
                        Servicios Principales
                    </header>
                    <div class="card-body">
                        @foreach($categories as $category)
                        <div class="form-group">
                            {!! Field::text('service1', $category->name ?? null, ['label' => 'Servicio ' . $loop->iteration, 'disabled' => 'disabled']) !!}
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        @include('admin.modals.loader')
    </section>
@endsection
@push('scripts')
    <script>
        $('.summernote').summernote({
            height: 200,
            popover: {
                image: [],
                link: [],
                air: []
            }
        });
    </script>
@endpush