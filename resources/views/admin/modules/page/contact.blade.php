@extends('admin.layouts.base')
@section('bodyId')id="setting" @endsection
@section('content')
    <section class="wrapper">
        <div class="row">
            <div class="col-12">
                <h3 class="title d-flex justify-content-between align-items-center">
                    Contacto
                </h3>
            </div>
        </div>
        {!! Form::open(['route' => 'admin.setting.store']) !!}
        <div class="row">
            {!! Field::hidden('section', \App\Setting::CONTACT) !!}
            <div class="col-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('phone1', $setting->data['phone1'] ?? null, ['label' => 'Teléfono 1', 'placeholder' => '']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('phone2', $setting->data['phone2'] ?? null, ['label' => 'Teléfono 2', 'placeholder' => '']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('email1', $setting->data['email1'] ?? null, ['label' => 'Email 1', 'placeholder' => '']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('email2', $setting->data['email2'] ?? null, ['label' => 'Email 2', 'placeholder' => '']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    {!! Field::text('address', $setting->data['address'] ?? null, ['label' => 'Dirección', 'placeholder' => '']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>

            </div>
            <div class="col-4">
                <div class="action d-flex justify-content-between">
                    <button type="submit" class="button dark active">Guardar</button>
                </div>
                <br>
            </div>
        </div>
        {!! Form::close() !!}
        @include('admin.modals.loader')
    </section>

@endsection
@push('scripts')
    <script>
        $('.summernote').summernote({
            height: 200,
            popover: {
                image: [],
                link: [],
                air: []
            }
        });
    </script>
@endpush
