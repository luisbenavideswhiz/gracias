@extends('admin.layouts.base')
@section('bodyId')id="setting" @endsection
@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-12">
                <h3 class="title">Datos de la empresa</h3>
            </div>
        </div>
        {!! Form::open(['route' => 'admin.setting.store']) !!}
        {!! Field::hidden('id', $setting->id ?? null) !!}
        {!! Field::hidden('section', \App\Setting::HOME) !!}
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('title', $setting->data['title'] ?? null, ['label' => 'Título Principal (Separe con / para salto de línea)*', 'placeholder' => 'Bienvenido']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('subtitle', $setting->data['subtitle'] ?? null, ['label' => 'Subtítulo (30 caracteres)*', 'placeholder' => 'A nuestro portal de contenido digital', 'maxlength' => 30]) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('button_main', $setting->data['button_main'] ?? null, ['label' => 'Botón principal', 'placeholder' => 'Nombre del botón']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('button_link', $setting->data['button_link'] ?? null, ['label' => 'Link', 'placeholder' => 'Link del botón']) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('link_name', $setting->data['link_name'] ?? null, ['label' => 'Enlace secundario', 'placeholder' => 'Nombre del enlace']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('link_second', $setting->data['link_second'] ?? null, ['label' => 'Link', 'placeholder' => 'Link de enlace']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title d-flex justify-content-between align-items-center">
                            Redes Sociales
                        </h5>
                        <div class="social">
                            <div class="form-group">
                                {!! Field::text('facebook', $setting->data['facebook'] ?? null, ['label' => 'Facebook', 'placeholder' => 'Link de facebook']) !!}
                            </div>
                            <div class="form-group">
                                {!! Field::text('twiter', $setting->data['twiter'] ?? null, ['label' => 'Twiter', 'placeholder' => 'Link de twiter']) !!}
                            </div>
                            <div class="form-group">
                                {!! Field::text('instagram', $setting->data['instagram'] ?? null, ['label' => 'Instagram', 'placeholder' => 'Link de instagram']) !!}
                            </div>
                            <div class="form-group">
                                {!! Field::text('linkedin', $setting->data['linkedin'] ?? null, ['label' => 'Linkedin', 'placeholder' => 'Link de linkedin']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="action d-flex justify-content-between">
                    <button type="submit" class="button dark active">Guardar</button>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>
    @include('admin.modals.loader')
@endsection
