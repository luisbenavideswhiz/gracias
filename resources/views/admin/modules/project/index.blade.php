@extends('admin.layouts.base')
@section('bodyId')id="project" @endsection
@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-12">
                <h3 class="title d-flex justify-content-between align-items-center">
                    Portafolio - Proyectos
                    <a href="{{ route('admin.project.create') }}" class="button dark">
                        Crear nuevo
                    </a>
                </h3>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="table-projects" class="table table-bordered">
                            <thead>
                            <tr>
                                <th class="text-center w60">Orden</th>
                                <th>Cliente</th>
                                <th>Campaña</th>
                                <th>Categoría</th>
                                <th class="text-center w120">Acción</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($projects as $project)
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td>{{ $project->client }}</td>
                                    <td>{{ $project->campaign }}</td>
                                    <td>{{ $project->category->name }}</td>
                                    <td class="text-center d-flex justify-content-around">
                                        <a href="{{ route('admin.project.edit', $project->id) }}" class="icon-edit">
                                            <i class="material-icons">edit</i></a>
                                        <a href="{{ route('admin.gallery.create', $project->id) }}" class="icon-edit">
                                            <i class="material-icons">image</i></a>
                                        <a href="javascript:void(0)" class="icon-delete delete-project" data-toggle="modal"
                                           data-target="#delete" data-action="{{ route('admin.project.destroy', $project->id) }}"><i class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('admin.modules.category.modal.create')
    @include('admin.modules.category.modal.edit')
    @include('admin.modals.delete', [
       'title' => 'Eliminar Proyecto',
       'message' => 'Esta seguro que desea eliminar esta proyecto',
   ])
@endsection
@push('scripts')
    <script>
        $('.delete-project').on('click', function () {
            $('#form-delete').attr('action', $(this).data('action'));
            $('#delete').modal('show')
        });

        $('#table-projects').DataTable({
            language: {'url': 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json'},
            searching: false,
        });
    </script>
@endpush