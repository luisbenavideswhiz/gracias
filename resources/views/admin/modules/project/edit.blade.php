@extends('admin.layouts.base')
@section('bodyId')id="project" @endsection
@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-12">
                <h3 class="title d-flex justify-content-between align-items-center">
                    Nuevo - Proyecto
                </h3>
            </div>
        </div>
        {!! Form::open(['route' => ['admin.project.update', $project->id], 'files' => true, 'method' => 'PUT']) !!}
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <header class="card-header d-flex justify-content-between">
                        Información principal
                        <div class="">
                            <input type="checkbox" name="outstanding" value="1" {{ $project->outstanding ? 'checked' : '' }}> Destacado
                        </div>
                    </header>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('client', $project->client ?? null, ['label' => 'Cliente', 'placeholder' => '']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('campaign', $project->campaign ?? null, ['label' => 'Campaña', 'placeholder' => '']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('implementation', $project->implementation ?? null, ['label' => 'Implementación', 'placeholder' => '']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::select('category_id', $categories, $project->category_id, ['label' => 'Categoría', 'placeholder' => '']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('year', $project->year ?? null, ['label' => 'Año', 'placeholder' => '']) !!}
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    {!! Field::text('place', $project->place ?? null, ['label' => 'Lugar', 'placeholder' => '']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <header class="card-header">
                        Descripción
                    </header>
                    <div class="card-body">
                        <textarea class="summernote" name="description">{{ $project->description }}</textarea>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="action d-flex justify-content-between">
                    <button type="submit" class="button dark active">Guardar</button>
                    <a href="{{ route('admin.project.index') }}" class="button red">Cancelar</a>
                </div>
                <br>
                <div class="card">
                    <header class="card-header">
                        Imagen de Portada
                    </header>
                    <div class="card-body">
                        La imagen debe medir al menos 599 x 476 px
                        @component('admin.partials.upload-image', [
                            'path' => $project->path_cover_image ?? '',
                            'width' => 350,
                            'height' => 280
                            ])
                            @slot('edit') cover_image @endslot
                        @endcomponent
                    </div>
                </div>
                <br>
                <div class="card">
                    <header class="card-header">
                        Etiqueta SEO (separe por comas)
                    </header>
                    <div class="card-body">
                        <div class="form-group">
                            <input class="form-control" id="seo" name="seo" type="text" value="{{ $project->seo }}">
                        </div>
                    </div>
                </div>
            </div>

            <br>
        </div>
        {!! Form::close() !!}
    </section>
    @include('admin.modals.loader')
    @include('admin.modals.cropper-image')
@endsection
