@extends('admin.layouts.base')
@section('bodyId')id="client" @endsection
@section('content')
    <section class="wrapper">
        <!-- page start-->
        <div class="row">
            <div class="col-12">
                <h3 class="title d-flex justify-content-between align-items-center">
                    Nosotros - Clientes
                </h3>
            </div>
        </div>
        {!! Form::open(['route' => 'admin.client.store', 'files' => true]) !!}
        {!! Form::hidden('id', $client->id ?? null) !!}
        <div class="row">
            <div class="col-8">
                <div class="card">
                    <header class="card-header">
                        Logo
                    </header>
                    <div class="card-body">

                        <div class="form-group">
                            {!! Field::text('link',  $client->link ?? null, ['label' => 'Link', 'placeholder' => '']) !!}
                        </div>
                        <div class="action d-flex justify-content-between">
                            <button type="submit" class="button dark active">Guardar</button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-body">
                        La imagen debe medir al menos 150 x 45 px
                        @component('admin.partials.upload-image', ['width' => 300, 'height' => 90, 'path' => $client->path_logo ?? null])
                            @slot('edit') logo @endslot
                        @endcomponent

                    </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
        <div class="row">
            <div class="col-12 mt-5">
                <div class="card">
                    <div class="card-body">
                        <table class="table table-bordered" id="table-clients">
                            <tbody>
                            @foreach($clients as $client)
                                <tr id="{{ $client->id }}">
                                    <td class="text-center index">{{ $loop->iteration }}</td>
                                    <td><img src="{{ $client->path_logo }}" width="80px"></td>
                                    <td>Link: {{ $client->link }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('admin.client.edit', $client->id) }}" class="icon-edit">
                                            <i class="material-icons">edit</i></a>
                                        <a href="javascript:void(0)" class="icon-delete delete-client"
                                           data-action="{{ route('admin.client.destroy', $client->id) }}"><i class="material-icons">delete</i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            @include('admin.modals.loader')
            @include('admin.modals.cropper-image')
            @include('admin.modals.delete', [
                  'title' => 'Eliminar Logo',
                  'message' => 'Esta seguro que desea eliminar esta logo'
              ])
        </div>
    </section>
@endsection
