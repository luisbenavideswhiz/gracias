<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.assets.metas')
    <link rel="stylesheet" href="{{ mix('css/admin/app.css') }}">
    @include('admin.assets.ie9')
</head>
<body @section('bodyId')@show>
<section id="container">
    @include('admin.partials.header')
    @include('admin.partials.aside')
    <section id="main-content">
        @yield('content')
    </section>
</section>
<script type="text/javascript" src="{{ mix('js/admin/app.js') }}"></script>
@stack('scripts')
</body>
</html>