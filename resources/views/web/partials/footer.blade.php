<div id="footer" data-aos="fade-up">
    <div class="button">
        <a href="#" class="btn btn-back-top">VOLVER ARRIBA</a>
        <span class="h-border"></span>
    </div>

    <p class="copy">GRACIAS <br><span class="line-y"> | </span> TODOS LOS DERECHOS RESERVADOS 2019</p>

    <p class="by">POWERED BY <br> <a href="https://whiz.pe/" target="_blank">WHIZ</a></p>
</div>
