<div class="interior-header">
    <div class="container main">
        <a class="logo" href="{{ route('home') }}">
            <img src="{{ asset('img/imagotipo.svg') }}" alt="GRACIAS">
        </a>

        <div class="redes">
            <a href="{{ $home->data['facebook'] ?? '' }}" target="_blank" class="facebook">
                <img src="{{ asset('img/icons/facebook.svg') }}" alt="Facebook">
            </a>
            <a href="{{ $home->data['instagram'] ?? '' }}" target="_blank" class="instagram">
                <img src="{{ asset('img/icons/instagram.svg') }}" alt="Instagram">
            </a>
        </div>
    </div>
</div>
