<header id="header" class="load-bottom {{ Route::is('home') ? 'black' : 'orange' }}">
    <div class="container">
        <div class="row nav-bar">
            <ul class="col menu">
                <li class="col">
                    <a class="{{ Route::is('home') ? 'active' : '' }}" href="{{ route('home') }}">INICIO</a>
                </li>
                <li class="col">
                    <a class="{{ Route::is('portfolio') || Route::is('portfolio-detail') ? 'active' : '' }}" href="{{ route('portfolio') }}">PORTAFOLIO</a>
                </li>
            </ul>

            <a class="col logo" href="{{ route('home') }}">
                @if ( Route::is('home') )
                <img class="image" src="{{ asset('img/logo-'."orange".'.svg') }}">
                @else
                <img class="image logo-black" src="{{ asset('img/logo-black.svg') }}">
                <img class="image logo-orange" src="{{ asset('img/logo-orange.svg') }}">
                @endif

                <img class="label" src="{{ asset('img/label-gracias.svg') }}">
            </a>

            <ul class="col menu">
                <li class="col">
                    <a class="{{ Route::is('us') ? 'active' : '' }}" href="{{ route('us') }}">NOSOTROS</a>
                </li>
                <li class="col">
                    <a class="{{ Route::is('contact') ? 'active' : '' }}" href="{{ route('contact') }}">CONTACTO</a>
                </li>
            </ul>

            <ul class="col data-mobile">
                <p>TELF: {{ $contact->data['phone1'] ?? ''}}</p>
                <a href="mailto:{{ $contact->data['email1'] ?? '' }}">{{ $contact->data['email1'] ?? '' }}</a>
            </ul>
        </div>

        <div class="nav-mobile">
            <div class="mobile-logo">
                <img class="image" src="{{ asset('img/logo-'."orange".'.svg') }}">

                <img class="label" src="{{ asset('img/label-gracias.svg') }}">
            </div>
            <div class="btn-menu-mobile">
                <div class="ico"></div>
            </div>
        </div>
    </div>
</header>
