@extends('web.layout.base')
@section('content')
    <div id="home" class="page-effect">
        <div class="parallax">
            <div class="box-parallax">
                {{-- fondo --}}
                <div class="cap cap1"></div>
                {{-- gracias --}}
                <div class="cap parallax-layer cap2" data-parallax-deep="150" data-parallax-position="transform"></div>
                {{-- casa --}}
                <div class="cap parallax-layer cap3" data-parallax-disallow="y" data-parallax-deep="250" data-parallax-position="transform"></div>
            </div>

            <div class="container">
                <div class="trama">
                    @for($x=0;$x<50;$x++)
                    <div class="circle"><div class="point"></div></div>
                    @endfor
                </div>
                <div class="main">
                    <div class="box-title">
                        <h1>
                            @foreach($titles as $title)
                            <span class="title-effect" data-title="{{ trim($title) }}">{{ trim($title) }}</span>
                            @endforeach
                            <span class="line"></span>
                        </h1>
                        <h2 class="description">{{ $home->data['subtitle'] ?? ''}}</h2>
                    </div>

                    <div class="button">
                        <a href="{{ $home->data['button_link'] }}" class="btn">{{ $home->data['button_main'] ?? ''}}</a>
                        <span class="h-border"></span>
                    </div>

                    <p class="partner">PARTNER <br>GRUPO MACLABI</p>

                    {{-- <p class="by">POWERED BY <br> <a href="https://whiz.pe/" target="_blank">WHIZ</a></p> --}}
                </div>
                <div class="redes">
                    <a href="{{ $home->data['facebook'] ?? '' }}" target="_blank" class="facebook">
                        <img src="{{ asset('img/icons/facebook.svg') }}" alt="Facebook">
                    </a>
                    <a href="{{ $home->data['instagram'] ?? '' }}" target="_blank" class="instagram">
                        <img src="{{ asset('img/icons/instagram.svg') }}" alt="Instagram">
                    </a>
                </div>
            </div>
        </div>

    </div>
@endsection
