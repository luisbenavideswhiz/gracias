@extends('web.layout.base')
@section('content')
    <div id="portfolio" class="page">
        <section id="data-page" class="data-page">
            @include('web.partials.interior-header')
            <div class="container content">
                <div class="row">
                    <div class="col-lg-10 title-page">
                        <h2 class="title-page-top title-effect" data-title="PORTAFOLIO" data-aos="fade-up">PORTAFOLIO <span class="line"></span></h2>
                    </div>
                </div>
            </div>
        </section>

        <section class="change-bg-menu portfolio-box section-rotate">
            <div class="container main">
                <div class="row filter">
                    <div class="col-md-2 filter-box">
                        <div class="dropdown">
                            <button class="dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $category->name ?? 'Categoría'}}</button>
                            <input type="hidden" name="category" value="">
                            <div class="dropdown-menu" aria-labelledby="category-slug">
                                @foreach($categories as $category)
                                <a class="dropdown-item {{ $category->slug == request('categoria') ? 'active' : '' }}" href="{{ route('portfolio', $category->slug) }}" data-value="">{{ $category->name }}</a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row  portfolio-card portfolio-list" data-aos="fade-up">
                    @foreach($projects as $project)
                    <div class="col-6 item {{ $project->outstanding ? 'featured' : '' }}">
                        <a class="item-content" href="{{ route('portfolio-detail', $project->slug) }}">
                            <div class="front">
                                <div class="content">
                                    <div class="name">
                                        <p>{{ $project->campaign }}</p>
                                    </div>
                                    <picture>
                                        {{-- <img src="{{ asset('img/img-portfolio.jpg') }}" alt=""> --}}
                                        <div class="image" style="background-image: url({{ $project->getPathCoverImage(484, 272) }})"></div>
                                    </picture>
                                </div>
                                {{-- <div class="description">{!! $project->description !!}</div> --}}
                            </div>

                            <div class="back">
                                <p>VER MÁS</p>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>

                @if($projects instanceof \Illuminate\Pagination\LengthAwarePaginator)
                <div class="row paginate">
                    <div class="ul">
                        @isset($projects->toArray()['prev_page_url'])
                        <a class="arrow left" href="{{ $projects->toArray()['prev_page_url'] }}">
                            <img src="{{ asset('img/icons/arrow-left-black.svg') }}">
                        </a>
                        @endisset
                        <div class="number">
                            <p>{{ $projects->currentPage() ?? '1' }}</p>
                            <span class="line"></span>
                            <p>{{ $projects->lastPage() ?? '1' }}</p>
                        </div>

                        @isset($projects->toArray()['next_page_url'])
                        <a class="arrow right" href="{{ $projects->toArray()['next_page_url'] }}">
                            <img src="{{ asset('img/icons/arrow-right-black.svg') }}">
                        </a>
                        @endisset
                    </div>
                </div>
                @endif
            </div>
        </section>

        <section class="footer">
            <div class="container">
                @include('web.partials.footer')
            </div>
        </section>

    </div>
@endsection
