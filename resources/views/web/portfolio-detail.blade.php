@extends('web.layout.base')
@section('content')
    <div id="portfolio" class="portfolio-detail page">
        <section id="data-page" class="data-page">
            @include('web.partials.interior-header')
            <div class="container content">
                <div class="row">
                    <div class="col-10 title-page">
                        <h2 class="title-page-top" data-aos="fade-up">
                            <span>{{ $project->client }}</span>
                            <p class="sub-title">Campaña: {{ $project->campaign }}</p>
                        </h2>
                        <div class="description" data-aos="fade-up">
                            <p>Categoría: {{ $project->category->name }}</p>
                            <p>Implementación: {{ $project->implementation }}</p>
                            <p>Año: {{ $project->year }}</p>
                            <p>Lugar: {{ $project->place }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="change-bg-menu article">
            <div class="container main">
                <div class="" data-aos="fade-up">
                {!! $project->description !!}
                @foreach($project->gallery()->orderBy('order','asc')->get() as $gallery)
                    @if($gallery->type == \App\Gallery::IMAGE)
                <img src="{{ $gallery->getPathImage(1168, 657)}}">
                       @else
                            <iframe width="1168" height="657"
                                    src="{{ $gallery->path_video }}">
                            </iframe>
                        @endif

                @endforeach
                </div>

            </div>
        </section>

        <section class="other-projects">
            <div class="container">
                <h2 class="title-section title-black" data-aos="fade-up">OTROS PROYECTOS</h2>
            </div>

            <div class="portfolio-card carousel" data-aos="fade-up">
                @foreach($projects as $project)
                <a class="item" href="{{ route('portfolio-detail', $project->slug) }}">
                    <div class="content">
                        <div class="name">
                            <p>{{ $project->client }}</p>
                        </div>
                        <picture>
                            {{-- <img src="{{ asset('img/img-portfolio.jpg') }}" alt=""> --}}
                            <div class="image" style="background-image: url({{ $project->getPathCoverImage(484, 272) }})"></div>
                        </picture>
                    </div>
                </a>
                @endforeach

            </div>
        </section>

        <section class="footer">
            <div class="container">
                @include('web.partials.footer')
            </div>
        </section>

    </div>
@endsection
