@extends('web.layout.base')
@section('content')
    <div id="contact" class="page page-thanks">
        <section id="data-page" class="data-page">
            @include('web.partials.interior-header')
            <div class="container content">
                <div class="row">
                    <div class="col-10 title-page">
                        <h2 class="title-page-top">CONTACTO</h2>
                        <p class="description">Estamos ansiosos por trabajar en tu próximo proyecto. Agendemos una reunión para ayudarte.</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="box-thanks" class="section-rotate box-thanks">
            <div class="container main">
                <div class="row">
                    <div class="col-10 mx-auto">
                        <div class="content">
                            <div class="box">
                                <div class="small">
                                    <img src="{{ asset('img/logo-black.svg') }}" width="48px">
                                </div>

                                <div class="big">
                                    <p class="label">¡GRACIAS POR ESCRIBIRNOS!</p>

                                    <span>Nos comunicaremos contigo a la brevedad.</span>
                                </div>
                            </div>
                            <a href="{{ route('portfolio') }}">IR AL PORTAFOLIO</a>
                        </div>
                    </div>
                </div>

                <div class="data">
                    <p class="telf">TELF: {{ $contact->data['phone1'] ?? '' }}</p>
                    <a class="mailto" href="mailto:contacto@gracias.pe"  target="_blank">{{ $contact->data['email1'] ?? '' }}</a>
                </div>
            </div>
        </section>
    </div>
@endsection
