@extends('web.layout.base')
@section('content')
    <div id="us" class="page">
        <section id="data-page" class="data-page">
            @include('web.partials.interior-header')
            <div class="container content">
                <div class="row">
                    <div class="col-lg-10 our-history">
                        <h2 class="title-page-top title-effect" data-title="NUESTRA HISTORIA" data-aos="fade-up">NUESTRA HISTORIA <span class="line"></span></h2>
                        <div class="row" data-aos="fade-up">
                            <div class="col-lg-6 col-12 description">
                                {!! $agency->data['history_left'] ?? '' !!}
                            </div>
                            <div class="col-lg-6 col-12 description">
                                {!! $agency->data['history_right'] ?? ''!!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="line-x-rotate"></div>

                <div class="row">
                    <div class="col-lg-10 mission-vision">
                        <div class="row">
                            <div class="col-lg-6 col-12 description" data-aos="fade-up">
                                <h2 class="title-effect" data-title="MISIÓN">MISIÓN</h2>
                                {!! $agency->data['mission'] ?? '' !!}
                            </div>

                            <div class="col-lg-6 col-12 description" data-aos="fade-up">
                                <h2 class="title-effect" data-title="VISIÓN">VISIÓN</h2>
                                {!! $agency->data['vision'] ?? '' !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="our-services" class="change-bg-menu section-rotate our-services">
            <div class="container main">
                <h2 class="title-section title-black title-effect" data-title="NUESTROS SERVICIOS PRINCIPALES" data-aos="fade-up">NUESTROS SERVICIOS PRINCIPALES <span class="line"></span></h2>

                <img class="dec-1" src="{{ asset('img/icons/decoration-2.png') }}">
                <img class="dec-2" src="{{ asset('img/icons/decoration-2.png') }}" width="92px">

                <div class="box-items" data-aos="fade-up">
                    <a class="item odd social" href="{{ route('portfolio', $categories[0]->slug ?? '#') }}">
                        <p>Ver trabajos</p>
                        <h2>{{ $categories[0]->name ?? '' }}</h2>
                    </a>

                    <a class="item even creation" href="{{ route('portfolio', $categories[1]->slug ?? '#') }}">
                        <h2>{{ $categories[1]->name ?? ''}}</h2>
                        <p>Ver trabajos</p>
                    </a>

                    <a class="item odd esthetic" href="{{ route('portfolio', $categories[2]->slug ?? '#') }}">
                        <p>Ver trabajos</p>
                        <h2>{{ $categories[2]->name ?? ''}}</h2>
                    </a>

                    <a class="item even realization" href="{{ route('portfolio', $categories[3]->slug ?? '#') }}">
                        <h2>{{ $categories[3]->name ?? ''}}</h2>
                        <p>Ver trabajos</p>
                    </a>
                </div>
            </div>
        </section>

        <section id="our-clients" class="our-clients">
            <div class="container">
                <h2 class="title-section title-white title-effect" data-title="NUESTROS CLIENTES" data-aos="fade-up">NUESTROS CLIENTES <span class="line"></span></h2>

                <div class="line-x-rotate"></div>

                {{-- <div class="decoration">
                    <img class="" src="{{ asset('img/icons/decoration-1.png') }}" width="122px">
                </div> --}}

                <div class="box-clients" data-aos="fade-up">
                    <div class="box">
                        @foreach($clients as $client)
                        <div class="item">
                            <img src="{{ $client->path_logo }}">
                        </div>
                        @endforeach
                    </div>
                </div>

                @include('web.partials.footer')

            </div>
        </section>
    </div>
@endsection
