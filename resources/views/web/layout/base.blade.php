<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1, user-scalable=no, minimal-ui">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{ asset('img/favicon.png') }}" type="image/png" sizes="32x32">

    <title>GRACIAS</title>
    <meta name="description" content="">

    <meta property="og:title" content="GRACIAS" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="{{ asset('img/facebook-share.png') }}" />

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="wrapper">
        <div class="preloader-page">
            <div class="loader">
                <div class="loader__box loader--left">
                    <img class="image" src="{{ asset('img/logo-white.svg') }}">
                </div>

                <div class="loader__box loader--right">
                    <img class="image" src="{{ asset('img/logo-orange.svg') }}">
                </div>
            </div>
            {{-- <div class="preloader-page-progress">
                <div class="line-y"></div>
                <p class="text">CARGANDO<span>...</span></p>
            </div> --}}
        </div>
        @include('web.partials.header')
        @yield('content')
        {{-- @include('web.partials.footer') --}}
    </div>
<script src="{{ mix('js/app.js') }}"></script>

@yield('script')
</body>
</html>
