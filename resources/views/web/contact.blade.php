@extends('web.layout.base')
@section('content')
    <div id="contact" class="page">
        <section id="data-page" class="data-page">
            @include('web.partials.interior-header')
            <div class="container content">
                <div class="row">
                    <div class="col-lg-10 title-page">
                        <h2 class="title-page-top title-effect" data-title="CONTACTO" data-aos="fade-up">CONTACTO <span class="line"></span></h2>
                        <p class="description" data-aos="fade-up">Estamos ansiosos por trabajar en tu próximo proyecto. Agendemos una reunión para ayudarte.</p>
                    </div>
                </div>
            </div>
        </section>

        <section id="box-form" class="change-bg-menu section-rotate box-form">
            <div class="container main">
                <form id="form-contact" class="form-contact" action="{{ route('save-contact') }}" data-aos="fade-up" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-8 mx-auto">
                            <div class="row">
                                <div class="form-group mb-4 col-sm-6 col-12">
                                    <div class="ui-field">
                                        <input id="name" class="ui-field-input iptText" type="text" name="first_name" autocomplete="off" required>
                                        <label class="ui-field-label" for="name">Nombre</label>
                                    </div>
                                </div>
                                <div class="form-group mb-4 col-sm-6 col-12">
                                    <div class="ui-field">
                                        <input id="last-name" class="ui-field-input iptText" type="text" name="last_name" autocomplete="off" required>
                                        <label class="ui-field-label" for="last-name">Apellido</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group mb-4 col-sm-6 col-12">
                                    <div class="ui-field">
                                        <input id="mobile" class="ui-field-input iptNumber" type="text" name="phone" autocomplete="off" maxlength="9" minlength="9" required>
                                        <label class="ui-field-label" for="mobile">Celular</label>
                                    </div>
                                </div>
                                <div class="form-group mb-4 col-sm-6 col-12">
                                    <div class="ui-field">
                                        <input id="email" class="ui-field-input" type="text" name="email" autocomplete="off" required>
                                        <label class="ui-field-label" for="email">Email</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group mb-4">
                                <div class="ui-field ui-textarea">
                                    <textarea id="message" class="ui-text" name="message" autocomplete="off" required=""></textarea>
                                    <label class="ui-field-label" for="message">Mensaje</label>
                                </div>
                            </div>

                            <div class="buttons">
                                <div class="button button-border-black">
                                    <button id="submit-contact" class="btn">ENVIAR</button>
                                    <span class="h-border"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                <div class="data">
                    <p class="telf">TELF: {{ $contact->data['phone1'] ?? ''}}</p>
                    <a class="mailto" href="mailto:contacto@gracias.pe"  target="_blank">{{ $contact->data['email1'] ?? ''}}</a>
                </div>
            </div>
        </section>

        <section id="locate-us" class="locate-us">
            <div class="container">
                {{-- <img class="decoration" src="{{ asset('img/icons/decoration-3.png') }}"> --}}

                <div class="line-x-rotate"></div>

                <div class="main">
                    <h2 class="title-section title-white title-effect" data-title="UBÍCANOS EN" data-aos="fade-up">UBÍCANOS EN <span class="line"></span></h2>
                    <p class="address" data-aos="fade-up">{{ $contact->data['address'] ?? '' }}</p>
                    <div id="map" class="map" data-aos="fade-up"></div>
                    @include('web.partials.footer')
                </div>
            </div>
        </section>
    </div>
@endsection

@section('script')
{{--MAP--}}
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCt3-aLL5MF1iaRTiFXgz8CeomI9xCeJcE&callback=initMap" async defer></script>
<script>
  let map, marker;

  function initMap() {
    let nLat = -12.1011771;
    let nLng = -76.9722429;
    map = new google.maps.Map(document.getElementById('map'), {
      center: new google.maps.LatLng(nLat, nLng),
      zoom: 15,
      streetViewControl: false,
      mapTypeControl: false,
    });
    marker = new google.maps.Marker({
      position: new google.maps.LatLng(nLat, nLng),
      animation: google.maps.Animation.DROP,
      map: map,
    });
  }
</script>
@endsection
