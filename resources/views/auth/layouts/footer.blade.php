<footer id="footer-login">
    <div class="footer-copy">
        <div class="container">
            <div class="text-center">
                <p>
                    <strong>Gracias</strong> | Todos los derechos reservados 2019 -
                    Powered by <a href="https://whiz.pe" target="_blank">Whiz</a>
                </p>
            </div>
        </div>
    </div>
</footer>