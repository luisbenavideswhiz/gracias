<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="whiz.pe">
    <meta name="keyword" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon" sizes="32x32">

    <title>Gracias</title>

    <link rel="stylesheet" href="{{ mix('css/admin/app.css') }}">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
</head>

<body id="login">
<div class="container">
    @yield('content')
    @include('auth.layouts.footer')
</div>
<script type="text/javascript" src="{{ mix('js/admin/app.js') }}"></script>
</body>
</html>
