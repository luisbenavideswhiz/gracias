@extends('mailing.layout.base')
@section('content')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td height="70"></td>
        </tr>
        <tr>
            <td>
                <h1 style="margin:0;font-family: Arial,serif;font-size:34px;font-weight:bold;color:#000;">HOLA,</h1>

                <h4 style="margin:15px 0 30px;font-family: Arial,serif;font-weight:bold;font-size:18px;color:#000;letter-spacing:1.21px;">RECIBISTE UN NUEVO MENSAJE WEB</h4>

                <span style="display:block;width:88px;height:8px;background:#FF3300"></span>

            </td>
        </tr>
        <tr>
            <td height="30"></td>
        </tr>
        <tr>
            <td>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td>
                            <P style="margin:15px 0;font-family: Arial,serif;font-size:18px;font-weight:bold;color:#000;letter-spacing:0.78px;margin:0;line-height:36px;">Nombre: <strong>{{ $data->first_name }}</strong></P>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <P style="margin:15px 0;font-family: Arial,serif;font-size:18px;font-weight:bold;color:#000;letter-spacing:0.78px;margin:0;line-height:36px;">Apellido: <strong>{{ $data->last_name }}</strong></P>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <P style="margin:15px 0;font-family: Arial,serif;font-size:18px;font-weight:bold;color:#FF3300;letter-spacing:0.78px;margin:0;line-height:36px;">Mail: <strong>{{ $data->email }}</strong></P>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <P style="margin:15px 0;font-family: Arial,serif;font-size:18px;font-weight:bold;color:#FF3300;letter-spacing:0.78px;margin:0;line-height:36px;">Celular: <strong>{{ $data->phone }}</strong></P>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <P style="margin:15px 0;font-family: Arial,serif;font-size:18px;font-weight:bold;color:#000;letter-spacing:0.78px;margin:0;line-height:36px;">Mensaje: <strong>{{ $data->message }}</strong></P>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td height="130"></td>
        </tr>
    </table>
@endsection
