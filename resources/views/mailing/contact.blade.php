@extends('mailing.layout.base')
@section('content')
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td height="65"></td>
        </tr>
        <tr>
            <td>
                <h1 style="margin:0;font-family: Arial,serif;font-size:34px;font-weight:bold;color:#000;text-align:center;text-transform: uppercase;">¡HOLA {{ $name }}!</h1>

                {{-- <h1 style="margin:0;font-family: Arial,serif;font-size:52px;font-weight:bold;color:#000;letter-spacing:2.25px;">HOLA demo demo,</h1> --}}

                <h2 style="margin:15px 0;font-family: Arial,serif;font-weight:bold;font-size:18px;color:#FF3300;line-height:32px;text-align:center;">RECIBIMOS TU MENSAJE CON ÉXITO.</h2>

                <p style="font-family: Arial,serif;font-weight:bold;font-size:18px;color:#000;line-height:24px;letter-spacing:0.78px;margin:24px auto 45px;max-width:70%;text-align:center;">Nos pondremos en contacto muy pronto. Mientras tanto, puedes conocer más de nosotros en:</p>
            </td>
        </tr>
        <tr>
            <td>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="120">
                    <tr>
                        <td>
                            <a href="https://www.facebook.com/graciaspe" target="_blank" style="display:block"><img src="{{ asset('img/mail/facebook-black.png') }}" alt="Facebook" height="36"></a>
                        </td>

                        <td>
                            <a href="#" target="_blank" style="display:block"><img src="{{ asset('img/mail/instagram-black.png') }}" alt="Instagram" height="36"></a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr>
            <td height="80"></td>
        </tr>

        <tr>
            <td>
                <p style="font-family: Arial,serif;font-weight:bold;font-size:18px;color:#000;line-height:24px;margin:0;text-align:center;">Hasta pronto</p>
            </td>
        </tr>

        <tr>
            <td height="90"></td>
        </tr>
    </table>
@endsection
