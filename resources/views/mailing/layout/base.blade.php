<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:v="urn:schemas-microsoft-com:vml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="format-detection" content="telephone=no"/><!--[if !mso]>
		<!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/><!--
		<![endif]--><!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <title></title>
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css" media="all">
        td[class="outlook-f"] {
            width: 244px !important;
        }

        td[class="outlook-c"] {
            width: 500px !important;
        }

        @media only screen and (max-width: 480px) {
            td[class="outlook-f"] {
                width: 100% !important
            }
        }
    </style>
    <![endif]-->
</head>
<body bgcolor="#fff" style="margin: 0; padding: 0; width: 100% !important;font-family: Arial,serif; font-size:">
<table align="center" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="100%">
    <tbody>
    <tr>
        <td>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="630" style="max-width:630px;width:100%">
                <tr>
                    <td>
                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="64" bgcolor="#000">
                            <tr>
                                <td align="center">
                                    <img src="{{ asset('img/mail/logo.png') }}" alt="GRACIAS">
                                </td>
                            </tr>
                        </table>

                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="">
                            <tr>
                                <td width="15"></td>
                                <td align="left">
                                    @yield('content')
                                </td>
                                <td width="15"></td>
                            </tr>
                        </table>

                        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" height="128" bgcolor="#000">
                            <tr>
                                <td align="center">
                                    <p style="font-family: Arial,serif;font-size:14px;color:#4A4A4A;font-weight:bold;line-height:21px;">© GRACIAS | Todos los derechos reservados 2019 <br>Powered by WHIZ</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>
