<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1, user-scalable=no, minimal-ui">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon" sizes="32x32">

    <title>GRACIAS</title>
    <meta name="description" content="">

    <meta property="og:title" content="Title" />
    <meta property="og:site_name" content="" />
    <meta property="og:url" content="" />
    <meta property="og:description" content="" />
    <meta property="og:image" content="" />

    <link href="{{ mix('css/app.css') }}" rel="stylesheet">

</head>
<body>
    <div id="wrapper">
        {{-- <div class="preloader-page">
            <img class="image" src="{{ asset('img/logo-orange.svg') }}">
            <div class="preloader-page-progress">
                <div class="line-y"></div>
                <p class="text">CARGANDO<span>...</span></p>
            </div>
        </div> --}}
        <section id="errors" class="errors">
            <div class="container">
                <div class="trama">
                    @for($x=0;$x<50;$x++)
                    <div class="circle"><div class="point"></div></div>
                    @endfor
                </div>
                @yield('content')
            </div>
        </section>
    </div>
<script src="{{ mix('js/app.js') }}"></script>

@yield('script')
</body>
</html>
