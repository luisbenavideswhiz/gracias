@extends('errors.layout.base')
@section('content')
<div class="main">
    <div class="row">
        <div class="col-lg-8 mx-auto box">
            <picture>
                <img src="{{ asset('img/logo-orange.svg') }}" alt="">
            </picture>

            <div class="data">
                <h2>ERROR 503</h2>
                <p>TUVIMOS UN PROBLEMA EN NUESTRO SERVIDOR</p>
            </div>
        </div>

        <div class="col-lg-8 mx-auto bottom">
            <p>Actualiza la página dentro de un rato.</p>
        </div>
    </div>
</div>
@endsection
