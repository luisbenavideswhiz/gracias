@extends('errors.layout.base')
@section('content')
<div class="main">
    <div class="row">
        <div class="col-lg-8 mx-auto box">
            <picture>
                <img src="{{ asset('img/logo-orange.svg') }}" alt="">
            </picture>

            <div class="data">
                <h2>ERROR 404</h2>
                <p>VAYA, HUBO UN PROBLEMA</p>
                <span>Parece que este enlace no es válido o está roto</span>
            </div>
        </div>

        <div class="col-lg-8 mx-auto bottom">
            <div class="button">
                <a href="{{ route('home') }}" class="btn">VOLVER AL INICIO</a>
                <span class="h-border"></span>
            </div>
        </div>
    </div>
</div>
@endsection
