<?php

return [
    'Message' => [
        'not_found' => 'Mensaje no encontrado'
    ],
    'Category' => [
        'not_found' => 'Categoría no encontrada',
        'already_exist' => 'Categoria ya existe',
        'has_posts' => 'Categoría tiene artículos relacionados',
        'success' => [
            'show' => 'Categoría encontrada con éxito!',
            'list' => 'Listado de categorías',
            'store' => 'Categoría creada con éxito!',
            'update' => 'Categoría actualizada con éxito!',
            'destroy' => 'Categoría eliminada con éxito!'
        ]
    ],
    'Gallery' => [
        'not_found' => 'Galeria no encontrada'
    ],
    'Product' => [
        'not_found' => 'Producto no encontrado',
        'not_published' => 'Producto no publicado',
        'success' => [
            'show' => 'Producto encontrado con éxito!',
            'list' => 'Listado de productos',
            'store' => 'Producto creado con éxito!',
            'update' => 'Producto actualizado con éxito!',
            'destroy' => 'Producto eliminado con éxito!',
            'store_many' => 'Productos guardados con éxito'
        ]
    ],
    'SpecialCollection' => [
        'not_found' => 'Colección especial no encontrada',
        'already_exist' => 'Colección especial ya existe',
        'has_products' => 'Colección especial tiene productos relacionados',
        'success' => [
            'show' => 'Colección especial encontrada con éxito!',
            'list' => 'Listado de colección especial',
            'store' => 'Colección especial creada con éxito!',
            'update' => 'Colección especial actualizada con éxito!',
            'destroy' => 'Colección especial eliminada con éxito!'
        ]
    ],
    'Collection' => [
        'not_found' => 'Colección no encontrada',
        'already_exist' => 'Colección ya existe',
        'has_categories' => 'Colección tiene categorias relacionadas',
        'success' => [
            'show' => 'Colección encontrada con éxito!',
            'list' => 'Listado de colección',
            'store' => 'Colección creada con éxito!',
            'update' => 'Colección actualizada con éxito!',
            'destroy' => 'Colección eliminada con éxito!'
        ]
    ],
    'ContactInfo' => [
        'not_found' => 'No se encontro la información de contacto',
        'success' => [
            'store' => 'Información de contacto registrada con éxito',
            'show' => 'Información de contacto mostrada con éxito'
        ]
    ],
    'Store' => [
        'not_found' => 'No se encontro tienda'
    ],
    'Sale' => [
        'not_found' => 'No se encontro pago',
        'success' => [
            'store' => 'Pago se realizo con éxito'
        ]
    ],
    'PaymentGateway' => [
        'not_found' => 'No se encontro pasarela de pago',
        'success' => [
            'show' => 'Pasarela encontrada con éxito'
        ]
    ],
    'Register' => [
        'not_found' => 'No se encontro la información de contacto',
        'success' => [
            'store' => 'Información de contacto registrada con éxito',
        ]
    ],
    'Payment' => [
        'not_working' =>'No se pudo procesar el pago',
        'success' => [
            'store' => 'Pago se proceso con éxito'
        ]
    ],
    'Auth' => [
        'login' => [
            'success' => 'Usuario validado con éxito',
            'error' => 'Usuario y/o contraseña es incorrecto.'
        ]
    ],
    'Invoice' => [
        'not_found' => 'Factura no encontrada',
        'already_exist' => 'Factura ya existe',
        'not_registered' => 'No se pudo registrar factura',
        'success' => [
            'store' => 'Factura creada con éxito!',
        ]
    ],
    'Address' => [
        'not_found' => 'Por el momento no llegamos a la dirección indicada. Te recomendamos recoger tu pedido en una de <span>nuestras tiendas.</span>',
        'success' => [
            'show' => 'La ubicación esta cubierta para nuestros envíos'
        ]
    ]
];
