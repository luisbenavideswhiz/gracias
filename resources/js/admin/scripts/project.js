require('../vendor/bootstrap-fileupload');
require('summernote');
window.Form = require('../vendor/form').default;
$(function () {
    $('.summernote').summernote({
        height: 200,
        popover: {
            image: [],
            link: [],
            air: []
        }
    });
    window.Form.form('form', true);
    window.Form.imageEditor('#cropper_modal', '.fileupload', '#image', '.modal-btn-confirm', '.fileupload-exists', 484, 272);
});