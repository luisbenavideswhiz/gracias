require('summernote');
window.Form = require('../vendor/form').default;
$(function () {
    $('.summernote').summernote({
        height: 200,
        popover: {
            image: [],
            link: [],
            air: []
        }
    });
    window.Form.form('form');
});