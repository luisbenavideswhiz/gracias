import toastr from "toastr";

require('../vendor/bootstrap-fileupload');
require('../vendor/jquery-ui');
window.Form = require('../vendor/form').default;
$(function () {
    window.Form.form('form', true);
    window.Form.imageEditor('#cropper_modal', '.fileupload', '#image', '.modal-btn-confirm', '.fileupload-exists', 1170, 658);

    //Sorteable
    let fixHelperModified = function(e, tr) {
            let $originals = tr.children();
            let $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        },
        updateIndex = function(e, ui) {
            $('td.index', ui.item.parent()).each(function (i) {
                $(this).html(i+1);
            });
            $('input[type=text]', ui.item.parent()).each(function (i) {
                $(this).val(i + 1);
            });
        };

    $("#table-gallery tbody").sortable({
        helper: fixHelperModified,
        stop: updateIndex
    }).disableSelection();

    $("tbody").sortable({
        distance: 5,
        delay: 100,
        opacity: 0.6,
        cursor: 'move',
        update: function(event, ui) {
            $.ajax({
                url: '/admin/gallery/order',
                type: 'post',
                data: {'start_id': ui.item.attr("id"), 'end_id': ui.item.next().attr("id") },
                beforeSend: function () {
                    $('.modal-loader').fadeIn();
                },
                success: function (response) {
                    toastr.success(response.message, 'Correcto');
                    window.location.href = response.data.redirect;
                }
            });
            /*$.post('/admin/gallery/order', {'start_id': ui.item.attr("id"), 'end_id': ui.item.next().attr("id") }).done(function(response) {
                toastr.success(response.message, 'Correcto');
                window.location.href = response.data.redirect;
            })*/
        }
    });
    function log(event, ui) {
        console.log(event.type,
            ui.item.prev().attr("id") || "begining",
            ui.item.attr("id"),
            ui.item.next().attr("id") || "end"
        );
    }
});