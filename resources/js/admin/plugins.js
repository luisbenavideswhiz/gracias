try {
  window.$ = window.jQuery = require('jquery');
  require('bootstrap')
} catch (e) {

}

require('./vendor/jquery.dcjqaccordion.2.7');
require('./vendor/jquery.scrollTo.min');
require('./vendor/jquery.nicescroll');
require('./vendor/jquery.sparkline');
require('./vendor/slidebars.min');
require('./vendor/common-scripts');
require('./vendor/jquery.tagsinput');
require('./vendor/sparkline-chart');
require('./vendor/bootstrap-switch');
require('./vendor/switchery');
require('./vendor/count');

require('./vendor/bootstrap-fileupload');