import toastr from 'toastr';

window.editors = [];
let Cropper = require('cropperjs');

let cropper, appends = [], imageEditorActive;

let appendDataForm = function (items, push = false) {
  push !== false ? appends.push(items) : appends = items;
};

let removeDataForm = function (item) {
  if (typeof item !== 'undefined') {
    let index = appends.findIndex(appends => appends.name === item);
    index !== -1 ? appends.splice(index, 1) : null;
  } else {
    appends = [];
  }
};

let ajaxMultipart = function (form, callback) {
  let data = new FormData(form[0]);
  if (typeof appends !== 'undefined' && appends.length !== 0) {
    $.each(appends, function (i, val) {
      data.append(val.name, val.value);
    });
  }
  if (typeof callback === 'undefined') {
    callback = function (response) {
      if (!response.status) {
        // $('.modal-loader').fadeOut();
        $.each(response.data, function (i, val) {
          console.log(val);
          toastr.error(val, 'Error');
          $('.modal-loader').fadeOut();
        });
      } else {
        if (typeof response.data.redirect !== 'undefined') {
          window.location.href = response.data.redirect;
        }
        toastr.success(response.message, 'Correcto');
      }
    };
  }
  return {
    url: form.prop('action'),
    type: form.prop('method'),
    data: data,
    cache: false,
    contentType: false,
    processData: false,
    beforeSend: function () {
      $('.modal-loader').fadeIn();
    },
    success: callback
  };
};

let ajaxSimple = function (form, callback) {
  if (typeof callback === 'undefined') {
    callback = function (response) {
      $('.modal-loader').fadeOut();
      if (!response.status) {
        toastr.error(response.message, 'Error');
        $.each(response.data, function (i, val) {
          toastr.error(val, 'Error');
        });
      } else {
        if (typeof response.data.redirect !== 'undefined') {
          window.location.href = response.data.redirect;
        }
        toastr.success(response.message, 'Correcto');
      }
    };
  }
  return {
    url: form.prop('action'),
    type: form.prop('method'),
    data: form.serialize(),
    beforeSend: function () {
      $('.modal-loader').fadeIn();
    },
    success: callback
  };
};

let submitForm = function (id, multipart, callback) {
  $(id).submit(function (event) {
    event.preventDefault();
    //CKEditorUpdate();
    updateTextEditors();
    let form = $(this);
    let ajaxType;
    multipart = multipart || false;
    if (multipart) {
      ajaxType = ajaxMultipart(form, callback);
    } else {
      ajaxType = ajaxSimple(form, callback);
    }
    $.ajax(ajaxType);
  });
};

let CKEditorUpdate = function () {
  if (typeof CKEDITOR !== 'undefined') {
    for (var instance in CKEDITOR.instances) {
      CKEDITOR.instances[instance].updateElement();
    }
  }
};

function imageCropper(image, _height, _width) {
  return new Cropper($(image)[0], {
    aspectRatio: _width / _height,
    minContainerWidth: 600,
    minContainerHeight: 480
  });
}

function imageEditor(modal, wrapper, image, btn, input, _width, _height) {
  console.log('img');
  let _this = this;
  $(input).click(function () {
    imageEditorActive = $(this).data('input');
  });

  $(modal).on('show.bs.modal', function () {
    $(image).prop('src', $(wrapper).find('.fileupload-preview.' + imageEditorActive + '>img').prop('src'));
    cropper = imageCropper(image, _height, _width);
  });

  $(btn).click(function () {
    let canvas = cropper.getCroppedCanvas();
    $(wrapper).find('.fileupload-preview.' + imageEditorActive + '>img').prop('src', canvas.toDataURL());

    $(modal).modal('hide');
    canvas.toBlob(function (blob) {
      _this.removeDataForm(imageEditorActive);
      _this.appendDataForm({name: imageEditorActive, value: blob}, true);
    });
  });
  $(modal).on('hide.bs.modal', function () {
    cropper.destroy();
  });
}

function selectSubmit(id) {
  $(id).change(function () {
    $(this).parents('form').submit();
  });
}

function appendTextEditor(editors) {
  window.editors.push(editors);
}

function updateTextEditors() {
  window.editors.forEach(value => {
    value.updateSourceElement();
  });
}

let Form = {
  form: submitForm,
  appendDataForm: appendDataForm,
  removeDataForm: removeDataForm,
  imageEditor: imageEditor,
  appendTextEditor: appendTextEditor,
  select: selectSubmit
};
export default Form;
