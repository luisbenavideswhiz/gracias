$(function(){

  const elem = $('.parallax');
  // // Magic happens here
  // function parallax(e) {
  //   let _w = window.innerWidth / 2;
  //   let _h = window.innerHeight / 2;
  //   let _mouseX = e.clientX;
  //   let _mouseY = e.clientY;
  //   let _depth1 = `${50 - (_mouseX - _w) * 0.01}% ${50 - (_mouseY - _h) * 0.01}%`;
  //   let _depth2 = `${50 - (_mouseX - _w) * 0.06}% ${50 - (_mouseY - _h) * 0.06}%`;
  //   let _depth3 = `${50 - (_mouseX - _w) * 0.03}% ${50 - (_mouseY - _h) * 0.03}%`;
  //   let x = `${_depth3}, ${_depth2}, ${_depth1}`;
  //   // console.log(x);
  //   console.log(_w,_h);
  //   console.log(_mouseX,_mouseY);
  //   // elem.style.backgroundPosition = x;
  //   elem.css('background-position', x);
  // }

  // // document.addEventListener("mousemove", parallax);
  // elem.on('mousemove', parallax);

  function parallax(e) {
    let el = $(this);

    let x = e.clientX,
        y = e.clientY;

    let layers = el.find('.parallax-layer');

    for (let i = 0; i < layers.length; i++) {

      let deep = $(layers[i]).attr('data-parallax-deep');
          disallow = $(layers[i]).attr('data-parallax-disallow'),
          position = $(layers[i]).attr('data-parallax-position'),
          itemX = (disallow && disallow === 'x') ? 0 : x / deep,
          itemY = (disallow && disallow === 'y') ? 0 : y / deep,
          dataT = `translateX(${itemX}%) translateY(${itemY}%)`,
          dataB = `${itemX}% ${itemY}%`,

          data = ( position && position === 'transform') ? dataT : dataB;

      if (disallow && disallow === 'both') return;

      $(layers[i]).css(position, data);

    }

  }

  elem.on('mousemove', parallax);

});
