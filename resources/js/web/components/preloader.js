$(function(){

  let win = $(window);

  win.on('load', (e) => {
    $('.preloader-page').fadeOut();
    $('#header').removeClass('load-bottom');
    $('#home').removeClass('page-effect');
    $('#home').addClass('page-animation');
  });

});
