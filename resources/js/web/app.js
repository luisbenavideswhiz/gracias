/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });

try {
  window.$ = window.jQuery = require('jquery');
} catch (e) {
}

// require('./scripts/form-validate');
require('slick-carousel');
require('./components/preloader');
require('./components/parallax');

// ONLY VIEW CONTACT
if ($('#form-contact').length) {
  require('./components/form');
  require('./components/form-validate');
}

if ( $('#portfolio').length ) {
  require('./bootstrap');
}

import AOS from 'aos';
import 'aos/dist/aos.css';

AOS.init();

$(function () {
  let win = $(window);
  let html = $('html,body');
  let scrollTop = $('.btn-back-top');

  // US
  let us = $('#us');
  let usHeader = us.find('.interior-header');
  let usHeaderMain = usHeader.find('.main');
  let ourServices = $('#our-services');
  let ourServicesTitle = ourServices.find('.title-section');

  scrollTop.on('click', (e)=> {
    e.preventDefault();

    html.animate({
      scrollTop: 0
    });
  });

  $('.carousel').slick({
    centerMode: true,
    centerPadding: '105px',
    slidesToShow: 3,
    dots: false,
    responsive: [
      {
        breakpoint: 1500,
        settings: {
          slidesToShow: 2
        }
      },
      {
        breakpoint: 801,
        settings: {
          slidesToShow: 1
        }
      },
      {
        breakpoint: 601,
        settings: {
          centerPadding: '50px',
          slidesToShow: 1
        }
      }
    ]
  });

  $('.btn-menu-mobile').on('click', function(e){
    let el = $(this);
    let parent = el.parent();

    if (parent.hasClass('active')) {
      parent.removeClass('active');
      $('.nav-bar').removeClass('active');
    } else {
      parent.addClass('active');
      $('.nav-bar').addClass('active');
    }

  });

  let changeBgMenu = $('.change-bg-menu');

  win.scroll( ()=> {
    let scroll = $(this).scrollTop();

    if ( $('#us').length ) {
      if ( scroll >= ourServicesTitle.offset().top ) {
        usHeaderMain.addClass('modifier');
      } else {
        usHeaderMain.removeClass('modifier');
      }
    }

    if (changeBgMenu.length) {
      // let changeBgMenuTop = changeBgMenu.height() - changeBgMenu.offset().top;
      let changeBgMenuTop = $('#header').height() - ( win.height() - changeBgMenu.position().top );

      // let changeBgMenuBottom = changeBgMenuTop + changeBgMenu.height();
      let changeBgMenuBottom = changeBgMenu.offset().top - ( win.height() - changeBgMenu.height() ) + $('#header').height();

      if ( scroll >= changeBgMenuTop && scroll <= changeBgMenuBottom  ) {
        $('#header').addClass('black');
        // $('#header').find('.image').attr('src','/img/logo-orange.svg');
      } else {
        $('#header').removeClass('black');
        // $('#header').find('.image').attr('src','/img/logo-black.svg');
      }

    }

  });



});
