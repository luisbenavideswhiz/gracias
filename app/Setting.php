<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected  $fillable = ['data', 'section'];
    protected $casts = ['data' => 'array', 'section' => 'string'];
    const HOME = 'home';
    const CONTACT = 'contact';
    const AGENCY = 'agency';

    public static function section($section)
    {
        return self::where('section', $section)->first();
    }
}
