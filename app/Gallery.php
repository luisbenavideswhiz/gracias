<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    const IMAGE = 'image';
    const VIDEO = 'video';

    public function getPathImage($width, $height, $format = 'png')
    {
        return isset($this->image) ?
            (strpos($this->image, 'http') !== false ?
                $this->image : config('services.whiz.images.path') . '/' . $this->image . '/' . $format . '/' . $width . '/' . $height ) : null;
    }

    public function getPathVideoAttribute()
    {
        return str_replace('watch?v=', 'embed/', $this->name);
    }
}
