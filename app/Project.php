<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = ['name', 'category_id', 'client', 'campaign', 'implementation', 'year', 'outstanding',
        'place', 'description', 'cover_image', 'seo', 'slug'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getPathCoverImage($width, $height, $format = 'png')
    {
        return isset($this->cover_image) ?
            ( strpos($this->cover_image, 'https') !== false ?
                $this->cover_image : config('services.whiz.images.path') . '/' . $this->cover_image . '/' . $format . '/' . $width . '/' . $height ) : null;
    }

    public function gallery()
    {
        return $this->hasMany(Gallery::class);
    }
}
