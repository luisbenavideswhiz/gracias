<?php

namespace App\Exceptions;

use App\Presenters\ResponsePresenter;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ServerException;

class Handler extends ExceptionHandler
{
    use ResponsePresenter;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson() || $request->ajax()) {
            switch (get_class($exception)) {
                case ValidationException::class:
                    if ($request->ajax()) {
                        $response = $this->responseError('validation_error', $exception->validator->errors(), $exception);
                    } else {
                        $response = $this->responseError('validation_error', $exception->validator->errors(), $exception, 422);
                    }
                    break;
                case AuthenticationException::class:
                    $response = $this->responseError(trans('auth.unauthenticated'), null, $exception);
                    break;
                case RequestException::class:
                    $response = $this->responseError($this->extractMessage($exception), $this->extractData($exception), null, 422);
                    break;
                case ClientException::class:
                    $response = $this->responseError($this->extractMessage($exception), $this->extractData($exception), null, 422);
                    break;
                case ServerException::class:
                    $response = $this->responseError($this->extractMessage($exception), $this->extractData($exception), null, 422);
                    break;
                default:
                    $response = $this->responseError($exception->getMessage(), null, $exception);
                    break;
            }
        } else {
            if ($exception instanceof TokenMismatchException) {
                $response = redirect()->route('login')
                    ->withErrors(['token' => trans('auth.token')]);
            } else {
                $response = parent::render($request, $exception);
            }
        }
        return $response;
    }
}
