<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Http\Requests\Setting\StoreRequest;
use App\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class SettingController extends Controller
{

    public function home()
    {
        $setting = Setting::section(Setting::HOME);
        return view('admin.modules.page.home', compact('setting'));
    }

    public function agency()
    {
        $setting = Setting::section(Setting::AGENCY);
        $categories = Category::where('featured', true)->get();
        return view('admin.modules.page.agency', compact('setting', 'categories'));
    }

    public function contact()
    {
        $setting = Setting::section(Setting::CONTACT);
        return view('admin.modules.page.contact', compact('setting'));
    }

    public function store(StoreRequest $request)
    {
        try{
            $data = $request->except('_token');
            $data['data'] = $data;
            $data['section'] = $request->section;
            Setting::updateOrCreate(['section' => $request->section], $data);
            return $this->responseSuccess('Información guardada correctamente', ['redirect' => '']);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, intentelo más tarde', ['redirect' => '']);
        }


    }

}
