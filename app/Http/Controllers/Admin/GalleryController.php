<?php

namespace App\Http\Controllers\Admin;

use App\Gallery;
use App\Project;
use App\Services\API\ApiWhiz;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GalleryController extends Controller
{
    protected  $apiWhiz;
    public function __construct(ApiWhiz $apiWhiz)
    {
        $this->apiWhiz = $apiWhiz;
    }

    public function create(Project $project)
    {
        $galleries = $project->gallery()->orderBy('order', 'asc')->get();
        return view('admin.modules.gallery.index', compact('project', 'galleries'));
    }

    public function store(Request $request, Project $project)
    {
        try{
            DB::beginTransaction();
            $gallery = new Gallery();
            $gallery->order = Gallery::where('project_id', $project->id)->count() + 1;
            $gallery->project_id = $project->id;

            if($request->has('gallery') && $request->type == Gallery::IMAGE){
                $gallery->type = Gallery::IMAGE;
                $gallery->name = $request->file('gallery')->getClientOriginalName();
                $gallery->save();
                $multipart[] = ['name' => 'image[]', 'contents' => fopen($request->file('gallery')->getRealPath(), 'r')];
                $dataImage = ['table_name' => 'galleries', 'table_key' => $gallery->id];
                $responseImage = $this->apiWhiz->storeImage($dataImage, $multipart);
                if ($responseImage->response->status) {
                    $path = $responseImage->response->data[0]->uuid ?? null;
                    $gallery->image = $path;
                    $gallery->save();
                    DB::commit();
                }
                return $this->responseSuccess('Imagen guardada correctamente', ['redirect' => url()->previous()]);
            }

            if($request->has('video') && $request->type == Gallery::VIDEO){
                $gallery->type = Gallery::VIDEO;
                $gallery->name = $request->video;
                $gallery->image = $request->video;
                $gallery->save();
                DB::commit();
                return $this->responseSuccess('Video guardado correctamente', ['redirect' => url()->previous()]);
            }
            return $this->responseError('Contenido requerido', ['redirect' => url()->previous()]);
        }catch (\Exception $e){
            DB::rollBack();
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, inténtelo más tarde', ['redirect' => route('admin.project.index')]);
        }

    }

    public function order(Request $request)
    {
        try{
            $gallery1 = Gallery::find($request->start_id);
            $gallery2 = Gallery::find($request->end_id);
            $start = $gallery1->order;
            $end = $gallery2->order;
            $gallery1->order = $end;
            $gallery2->order = $start;
            $gallery1->save();
            $gallery2->save();
            return $this->responseSuccess('Ordenado correctamente', ['redirect' => url()->previous()]);

        }catch (\Exception $e){
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, intentelo más tarde', ['redirect' => '']);
        }
    }

    public function destroy($id)
    {
        try{
            $gallery = Gallery::find($id);
            $gallery->delete();
            return $this->responseSuccess('Eliminado correctamente', ['redirect' => url()->previous()]);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, intentelo más tarde', ['redirect' => '']);
        }
    }
}
