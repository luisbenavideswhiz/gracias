<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('admin.modules.category.index', compact('categories'));
    }

    public function store(Request $request)
    {
        try{
            $count_featured = Category::whereFeatured(true)->count();
            if(isset($request->featured) && $count_featured == 4){
                return $this->responseError('Solo puede destacar hasta 4 categorías');
            }
            Category::updateOrCreate(['id' => $request->id], [
                    'name' => $request->name,
                    'featured' => isset($request->featured),
                    'slug' => str_replace(' ', '-', strtolower($request->name))
                ]);
            return $this->responseSuccess('Guardado correctamente', ['redirect' => route('admin.category.index')]);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, inténtelo más tarde', ['redirect' => route('admin.category.index')]);
        }

    }

    public function destroy(Category $category)
    {
        try{
            $category->delete();
            return $this->responseSuccess('Eliminado correctamente', ['redirect' => route('admin.category.index')]);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            return $this->responseError('No se puede eliminar, cuenta con trabajos asignados', ['redirect' => route('admin.category.index')]);
        }
    }
}
