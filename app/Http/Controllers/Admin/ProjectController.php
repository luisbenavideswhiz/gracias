<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use App\Gallery;
use App\Http\Requests\Project\StoreRequest;
use App\Http\Requests\Project\UpdateRequest;
use App\Project;
use App\Http\Controllers\Controller;
use App\Services\API\ApiWhiz;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProjectController extends Controller
{
    protected  $apiWhiz;
    public function __construct(ApiWhiz $apiWhiz)
    {
        $this->apiWhiz = $apiWhiz;
    }

    public function index(){

        $projects = Project::orderBy('id', 'desc')->get();
        return view('admin.modules.project.index', compact('projects'));
    }

    public function create()
    {
        $categories = Category::pluck('name', 'id')->toArray();
        return view('admin.modules.project.create', compact('categories'));
    }

    public function store(StoreRequest $request)
    {
        try{
            DB::beginTransaction();
            $data = $request->except('_token');
            $data['outstanding'] = isset($request->outstanding);
            $data['slug'] = $this->createSlug($request);
            $project = Project::create($data);
            if($request->has('cover_image')){
                $multipart[] = ['name' => 'image[]', 'contents' => fopen($request->file('cover_image')->getRealPath(), 'r')];
                $dataImage = ['table_name' => 'projects', 'table_key' => $project->id];
                $responseImage = $this->apiWhiz->storeImage($dataImage, $multipart);
                if ($responseImage->response->status) {
                    $path = $responseImage->response->data[0]->uuid ?? null;
                    $project->cover_image = $path;
                    $project->save();
                }
            }
            DB::commit();
            return $this->responseSuccess('Información guardada correctamente', ['redirect' => route('admin.project.index')]);
        }catch (\Exception $e){
            DB::rollBack();
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, inténtelo más tarde', ['redirect' => route('admin.project.index')]);
        }

    }

    private function createSlug($request)
    {
        $slug = str_replace(' ', '-', strtolower($request->client)) . '-' . str_replace(' ', '-', strtolower($request->campaign)) .
            '-' . $request->year;
        return strtolower($slug);
    }

    public function edit(Project $project)
    {
        $categories = Category::pluck('name', 'id')->toArray();
        return view('admin.modules.project.edit', compact('project', 'categories'));
    }

    public function update(UpdateRequest $request, Project $project)
    {
        try{
            $data = $request->except('_token');
            $data['outstanding'] = isset($request->outstanding);
            $project->update($data);
            if($request->has('cover_image')){
                $multipart[] = ['name' => 'image[]', 'contents' => fopen($request->file('cover_image')->getRealPath(), 'r')];
                $dataImage = ['table_name' => 'projects', 'table_key' => $project->id];
                $responseImage = $this->apiWhiz->storeImage($dataImage, $multipart);
                if ($responseImage->response->status) {
                    $path = $responseImage->response->data[0]->uuid ?? null;
                    $project->cover_image = $path;
                    $project->save();
                }
            }
            return $this->responseSuccess('Información guardada correctamente', ['redirect' => route('admin.project.index')]);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, inténtelo más tarde', ['redirect' => route('admin.project.index')]);
        }
    }

    public function destroy(Project $project)
    {
        try{
            DB::beginTransaction();
            Gallery::where('project_id', $project->id)->delete();
            $project->delete();
            DB::commit();
            return $this->responseSuccess('Eliminado correctamente', ['redirect' => route('admin.project.index')]);
        }catch (\Exception $e){
            DB::rollBack();
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, inténtelo más tarde', ['redirect' => route('admin.project.index')]);
        }
    }
}
