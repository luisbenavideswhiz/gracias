<?php

namespace App\Http\Controllers\Admin;

use App\Client;
use App\Http\Requests\Client\StoreRequest;
use App\Services\API\ApiWhiz;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class ClientController extends Controller
{
    protected $apiWhiz;

    public function __construct(ApiWhiz $apiWhiz)
    {
        $this->apiWhiz = $apiWhiz;
    }

    public function index(){

        $clients = Client::orderBy('order', 'asc')->get();
        return view('admin.modules.client.index', compact('clients'));
    }

    public function store(StoreRequest $request)
    {
        try{
            if(isset($request->id)){
                $client = Client::find($request->id);
                $client->link = $request->link;
                $client->save();
            }else{
                $data = $request->except('_token');
                $data['order'] = Client::count() + 1;
                $client = Client::create($data);
            }

            if($request->has('logo')){
                $multipart[] = ['name' => 'image[]', 'contents' => fopen($request->file('logo')->getRealPath(), 'r')];
                $dataImage = ['table_name' => 'client', 'table_key' => $client->id];
                $responseImage = $this->apiWhiz->storeImage($dataImage, $multipart);
                if ($responseImage->response->status) {
                    $path = $responseImage->response->data[0]->uuid ?? null;
                    $client->logo = $path;
                    $client->save();
                }
            }
            return $this->responseSuccess('Información guardada correctamente', ['redirect' => route('admin.client.index')]);

        }catch (\Exception $e){
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, intentelo más tarde', ['redirect' => '']);
        }
    }

    public function edit(Client $client)
    {
        $clients = Client::orderBy('order', 'asc')->get();
        return view('admin.modules.client.index', compact('client', 'clients'));
    }

    public function update()
    {

    }
    public function destroy(Client $client)
    {
        try{
            $client->delete();
            return $this->responseSuccess('Eliminado correctamente', ['redirect' => route('admin.client.index')]);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, intentelo más tarde', ['redirect' => '']);
        }
    }

    public function order(Request $request)
    {
        try{
            $client1 = Client::find($request->start_id);
            $client2 = Client::find($request->end_id);
            $start = $client1->order;
            $end = $client2->order;
            $client1->order = $end;
            $client2->order = $start;
            $client1->save();
            $client2->save();
            return $this->responseSuccess('Ordenado correctamente', ['redirect' => route('admin.client.index')]);

        }catch (\Exception $e){
            Log::error($e->getMessage());
            return $this->responseError('Ocurrió un error, intentelo más tarde', ['redirect' => '']);
        }
    }
}
