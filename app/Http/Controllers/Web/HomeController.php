<?php

namespace App\Http\Controllers\Web;

use App\Category;
use App\Client;
use App\Message;
use App\Project;
use App\Services\API\ApiWhiz;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    protected $contact;
    protected $home;

    public function __construct()
    {
        $this->contact = Setting::section(Setting::CONTACT);//Se repite en todos, esta en el header
        $this->home = $home = Setting::section(Setting::HOME);
    }

    public function home()
    {
        try{
            $data = [
                'home' => $this->home,
                'titles' => explode('/', $this->home->data['title']),
                'contact' => $this->contact
            ];
            return view('web.index', $data);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            abort(500);
        }
    }

    public function agency()
    {
        try{
            $data = [
                'home' => $this->home,
                'agency' => Setting::section(Setting::AGENCY),
                'clients' => Client::orderBy('order')->get(),
                'categories' => Category::whereFeatured(true)->get(),
                'contact' => $this->contact
            ];
            return view('web.us', $data);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            abort(505);
        }
    }

    public function contact()
    {
        try{
            $data = [
                'home' => $this->home,
                'contact' => $this->contact
            ];
            return view('web.contact', $data);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            abort(500);
        }
    }

    public function project(Request $request)
    {
        try{
            $category = Category::whereSlug($request->slug)->first();
            if(isset($request->slug) && !isset($category)){
                return view('errors.404');
            }
            $data = [
                'home' => $this->home,
                'contact' => $this->contact,
                'category' => $category,
                'categories' => Category::whereHas('projects', function (){}, '>', 0)->get(),
                'projects' => isset($category->id) ?
                    Project::where('category_id', $category->id)->orderBy('outstanding', 'desc')->orderBy('id', 'desc')
                        ->paginate(6, ['*'], 'page', $request->page ?? 1) :
                    Project::where('outstanding', true)->orderBy('outstanding', 'desc')->orderBy('id', 'desc')->paginate(6, ['*'], 'page', $request->page ?? 1)
            ];
            //dd($data)
            return view('web.portfolio', $data);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            abort(500);
        }
    }

    public function detail($slug)
    {
        try{
            $project = Project::where('slug', $slug)->first();
            if(!isset($project)){
                return view('errors.404');
            }
            $data = [
                'home'      => $this->home,
                'contact'   => $this->contact,
                'project'   => $project,
                'projects'  => Project::inRandomOrder()->limit(4)->get()
            ];
            return view('web.portfolio-detail', $data);
        }catch (\Exception $e){
            Log::error($e->getMessage());
            abort(500);
        }
    }

    public function saveContact(ApiWhiz $apiWhiz, Request $request)
    {
        try{
            $post = $request->except('_token');
            Message::create($post);
            //Envio al usuario
            $apiWhiz->prepareMail('Gracias - Tus datos han sido registrado con éxito',
                ['email' => env('MAIL_FROM_ADDRESS'), 'name' => env('MAIL_FROM_NAME')],
                ['email' => $request->email, 'name' => $request->first_name])
                ->setView('mailing.contact', [
                    'name' => $request->first_name,
                ])->sendMail();
            //Envio al administrador
            $apiWhiz->prepareMail('Gracias - Tenemos un nuevo registro de contacto',
                ['email' => env('MAIL_FROM_ADDRESS'), 'name' => env('MAIL_FROM_NAME')],
                ['email' => $request->email, 'name' => $request->first_name])
                ->setView('mailing.admin', [
                    'data' => $request,
                ])->sendMail();

            return redirect()->route('thanks');

        }catch (\Exception $e){
            Log::error($e->getMessage());
            abort(500);
        }
    }

    public function thanks()
    {
        $data = [
            'home'      => $this->home,
            'contact'   => $this->contact,
        ];
        return view('web.contact-thanks', $data);
    }
}
