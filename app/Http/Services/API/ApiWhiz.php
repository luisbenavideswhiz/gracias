<?php

namespace App\Services\API;

use App\Http\Clients\HttpClient;
use Exception;
use Illuminate\Support\Facades\Cache;

class ApiWhiz
{

    private $client;
    private $url;
    private $clientId;
    private $clientToken;
    private $accessToken;

    private $sender;
    private $addressee;
    private $subject;
    private $template;
    private $cc;

    /**
     * ApiWhiz constructor.
     * @throws Exception
     */
    public function __construct()
    {
        $this->client = app()->make(HttpClient::class);
        $this->url = config('services.whiz.gateway_base_url');
        $this->clientId = config('services.whiz.client_id');
        $this->clientToken = config('services.whiz.client_secret');
        $this->accessToken = $this->getAccessToken();
    }

    /**
     * @throws Exception
     */
    private function getAccessToken()
    {
        if(!Cache::has('access_token_whiz')){
            $response = $this->client->post($this->url.'/oauth/token', [
                'grant_type' => 'client_credentials',
                'client_id' => $this->clientId,
                'client_secret' => $this->clientToken,
                'scope' => '*'
            ]);
            $expire = $response->response->expires_in;
            if(!isset($expire)){
                throw new Exception('Not working oauth');
            }
            cache(['access_token_whiz' => $response->response->access_token], now()->addSeconds($expire));
        }
        return cache('access_token_whiz');
    }

    //IMAGES
    /**
     * @param array $data
     * @param array $images
     * @param array $sizes
     * @return mixed
     */
    public function storeImage($data = [], $images = [], $sizes = [])
    {
        $this->client->addHeader('Authorization', 'Bearer '.$this->accessToken);
        $this->client->removeHeader('Content-Type');
        $newData = [];
        foreach ($data as $key => $value)
        {
            $newData[] = ['name' => $key, 'contents' => $value];
        }
        foreach($sizes as $i => $size){
            foreach ($size as $key => $value){
                $newData[] = ['name' => 'sizes['.$i.']['.$key.']', 'contents'=> $value];
            }
        }
        foreach ($images as $image)
        {
            $newData[] = $image;
        }
        return $this->client->post($this->url.'/v1/image', $newData, false, true);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function decodeByUrl($data)
    {
        $this->client->addHeader('Authorization', 'Bearer '.$this->accessToken);
        return $this->client->post($this->url.'/v1/image/decode', $data);
    }
    //END IMAGES

    // MAILING
    /**
     * @param $sender
     * @param $addressee
     * @param $subject
     * @param $template
     * @return $this
     * @throws \Throwable
     */
    public function prepareMail($subject, $sender, $addressee, $template = null)
    {
        $this->sender = $sender;
        $this->addressee = $addressee;
        $this->subject = $subject;
        if(!is_null($template)){
            $this->template = $template;
        }
        return $this;
    }

    /**
     * @param $view
     * @param $data
     * @return ApiWhiz
     * @throws \Throwable
     */
    public function setView($view, $data = [])
    {
        $this->template = view($view, $data)->render();
        return $this;
    }

    public function addCC($email, $name)
    {
        $this->cc[] = ['email' => $email, 'name' => $name];
        return $this;
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function sendMail()
    {
        $this->client->addHeader('Authorization', 'Bearer '.$this->accessToken);
        throw_if(is_null($this->template), new Exception('view is undefined'));
        $data = [
            'sender' => $this->sender,
            'addressee' => $this->addressee,
            'subject' => $this->subject,
            'template' => $this->template
        ];
        if(!is_null($this->cc) && count($this->cc) > 0){
            $data['cc'] = $this->cc;
        }
        return $this->client->post($this->url.'/v1/mail/send', $data);
    }

    //END MAILING
}
