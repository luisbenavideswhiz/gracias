<?php

namespace App\Http\Requests\Project;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @param Request $request
     * @return array
     */
    public function rules(Request $request)
    {
        $request->description = strip_tags($request->description);
        return [
            'client'            => 'required',
            'campaign'          => 'required',
            'implementation'    => 'required',
            'category_id'       => 'required|exists:categories,id',
            'year'              => 'required|numeric',
            'place'             => 'required',
            'description'       => 'required',
            'seo'               => 'required',
            'cover_image'       => 'required'
        ];
    }

    public function attributes()
    {
        return [
            'client'            => 'cliente',
            'campaign'          => 'campaña',
            'implementation'    => 'implementación',
            'category_id'       => 'categoría',
            'year'              => 'año',
            'place'             => 'lugar',
            'description'       => 'descripción',
            'cover_image'       => 'imagen portada'
        ];
    }
}
