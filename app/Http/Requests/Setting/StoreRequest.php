<?php

namespace App\Http\Requests\Setting;

use App\Setting;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->section){
            case Setting::HOME : return [
                'title'     => 'required',
                'subtitle'  => 'required'
            ];
            break;
            case Setting::AGENCY :
                $this->history = strip_tags($this->history);
                $this->vision = strip_tags($this->vision);
                $this->mission = strip_tags($this->mission);
                return [
                'history_left'      => 'required',
                'history_right'     => 'required',
                'vision'            => 'required',
                'mission'           => 'required',
            ];
            case Setting::CONTACT : return [
                'phone1'    => 'required|numeric',
                'phone2'    => 'nullable|numeric',
                'email1'    => 'required|email',
                'email2'    => 'nullable|email',
                'address'   => 'required'
            ];
            break;
            default: return [];
        }
    }

    public function attributes()
    {
        return [
            'title'         => 'título principal',
            'subtitle'      => 'subtítulo',
            'history_right' => 'historia sección derecha',
            'history_left'  => 'historia sección izquierda',
            'vision'    => 'visión',
            'mission'   => 'misión',
            'service1'  => 'servicio 1',
            'service2'  => 'servicio 2',
            'service3'  => 'servicio 3',
            'service4'  => 'servicio 4',
            'phone1'    => 'teléfono 1',
            'phone2'    => 'teléfono 2',
            'email1'    => 'email 1',
            'email2'    => 'email 2',
            'address'   => 'direccíon'
        ];
    }
}
