<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Client extends Model
{
    protected $fillable = ['logo', 'order', 'link'];

    public function getPathLogoAttribute()
    {
        return isset($this->logo) ? config('services.whiz.images.path') . '/' . $this->logo . '/webp' : null;
    }
}
