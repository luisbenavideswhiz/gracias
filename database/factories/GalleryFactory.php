<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Gallery::class, function (Faker $faker) {
    return [
        'name' => $faker->text(20),
        'image' => $faker->imageUrl(1168, 657, 'business'),
        'order' => $faker->randomNumber(3),
        'project_id' => \App\Project::all()->random()->id,
        'type'  => \App\Gallery::IMAGE
    ];
});
