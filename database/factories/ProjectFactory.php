<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Project::class, function (Faker $faker) {
    return [
        'client' => $faker->company,
        'campaign' => $faker->text(50),
        'implementation' => $faker->text(50),
        'year' => $faker->numberBetween(2001,2019),
        'place' => $faker->address,
        'outstanding' => rand(0,1),
        'description' => $faker->text,
        'cover_image' => $faker->imageUrl(484, 272, 'transport'),
        'seo' => $faker->text(5),
        'category_id' => \App\Category::all()->random()->id,
        'slug' => $faker->slug
    ];
});
