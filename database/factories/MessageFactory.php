<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Message::class, function (Faker $faker) {
    return [
        'first_name'    => $faker->name,
        'last_name'     => $faker->lastName,
        'phone'         => $faker->phoneNumber,
        'email'         => $faker->email,
        'message'       => $faker->realText(rand(200,300))
    ];
});
