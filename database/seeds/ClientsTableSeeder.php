<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("INSERT INTO `clients` (`id`,`logo`,`link`,`order`,`created_at`,`updated_at`) VALUES (1,'4c3c6401-503b-4f4b-b9e6-8823b6901118','https://www.airbnb.com.pe',1,'2019-09-04 17:06:28','2019-09-04 17:06:29')");
        DB::statement("INSERT INTO `clients` (`id`,`logo`,`link`,`order`,`created_at`,`updated_at`) VALUES (2,'8602747e-92ed-4d99-914e-80cf20989082','https://slack.com/intl/es-pe/',2,'2019-09-04 17:07:11','2019-09-04 17:07:11');");
        DB::statement("INSERT INTO `clients` (`id`,`logo`,`link`,`order`,`created_at`,`updated_at`) VALUES (3,'f2c87000-d731-4a84-8be6-e5b71cafa4db','https://mailchimp.com/',3,'2019-09-04 17:08:05','2019-09-04 17:08:05')");
        DB::statement("INSERT INTO `clients` (`id`,`logo`,`link`,`order`,`created_at`,`updated_at`) VALUES (4,'1ca3794f-c9db-4bd5-b0d2-0e2bd8a88ffb','https://www.dropbox.com',4,'2019-09-04 17:09:14','2019-09-04 17:09:15');");
        DB::statement("INSERT INTO `clients` (`id`,`logo`,`link`,`order`,`created_at`,`updated_at`) VALUES (5,'a7c79c22-7b0a-4a60-9035-8aed777ec143','https://ueno.co/',5,'2019-09-04 17:10:25','2019-09-04 17:10:26');");
        DB::statement("INSERT INTO `clients` (`id`,`logo`,`link`,`order`,`created_at`,`updated_at`) VALUES (6,'49b4d958-2ef7-4595-a3de-3c17ba0c1fae','https://www.pinterest.es',6,'2019-09-04 17:11:01','2019-09-04 17:11:02')");
        DB::statement("INSERT INTO `clients` (`id`,`logo`,`link`,`order`,`created_at`,`updated_at`) VALUES (7,'aaca2c29-9bc0-4461-939e-3d4175e22480','http://www.starbucks.com.pe/',7,'2019-09-04 17:11:38','2019-09-04 17:11:40');");
    }


}
