<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Setting::create([
            'data' => json_decode('{"title": "GRACIAS POR / LLEGAR / HASTA AQUÍ", "twiter": null, "section": "home", "facebook": "https://www.facebook.com/graciaspe", "linkedin": null, "subtitle": "Neque porro quisquam est qui dolorem dolit", "instagram": "http://gracias.fake/instagram", "link_name": null, "button_link": "http://gracias.fake/portafolio", "button_main": "NUESTRO TRABAJO", "link_second": null}'),
            'section' => 'home',
        ]);
        \App\Setting::create([
            'data' => json_decode('{"vision": "<p>Ser un referente de la publicidad a nivel mundial gracias a nuestra eficiencia, ética, creatividad y donde los clientes puedan depositar toda su confianza.</p>", "mission": "<p>Ser una agencia de publicidad que brinde soluciones estratégicas y creativas que supere las expectativas de nuestros clientes para volvernos un aliado vital para su marca.</p>", "section": "agency", "service1": "SOCIAL MEDIA", "service2": "CREACIÓN DE CONTENIDO", "service3": "ESTÉTICA DIGITAL", "service4": "REALIZACIÓN AUDIO VISUAL", "history_left": "<p> <strong>GRACIAS</strong> fue creado por el grupo Maclabi con el objetivo de resolver la necesidad de las marcas que quieren generar ventas, exposición de marca y posicionamiento en el mercado actual, con creatividad, estrategia y ejecución medible que permita seguir avanzando con los objetivos trazados.</p>\r\n\r\n                                <p><strong>¿Cómo empezó el grupo Maclabi?</strong> <br> Nació ante la falencia de empresas de representación de marcas y/o servicios profesionales, sólidas, con tecnología, experiencia y conocimiento de mercado. Esto brindaría un servicio de excelencia con resultados medibles que permitiría</p>", "history_right": "<p>a sus socios/propietarios de marcas y/o productos/servicios, una adecuada rentabilidad en sus negocios.</p>\r\n\r\n                                <p>Esta falencia fue encontrada y vista como una oportunidad por su fundador, quien cuenta con más de 27 años de experiencia previa en el área comercial de grandes empresas de consumo masivo y de servicios, para finalmente crear el Grupo Maclabi.</p>\r\n\r\n                                <p>En el año 2019 nace <strong>GRACIAS</strong>, una agencia publicitaria que brinda soluciones estratégicas y creativas, para convertirse en un aliado para todos sus clientes.</p>"}'),
            'section' => 'agency',
        ]);
        \App\Setting::create([
            'data' => json_decode('{"email1": "contacto@gracias.pe", "email2": null, "phone1": "393195227", "phone2": null, "address": "Av. El Polo 709, Santiago de Surco", "section": "contact"}'),
            'section' => 'contact',
        ]);
    }
}
