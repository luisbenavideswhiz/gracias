<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('admin', function (){
    return redirect()->route('login');
});


Route::namespace('Web')->group(function (){
    Route::get('gracias', 'HomeController@thanks')->name('thanks');
    Route::get('/', 'HomeController@home')->name('home');
    Route::get('nosotros', 'HomeController@agency')->name('us');
    Route::get('contacto', 'HomeController@contact')->name('contact');
    Route::get('portafolio/{slug?}', 'HomeController@project')->name('portfolio');
    Route::get('/{slug}', 'HomeController@detail')->name('portfolio-detail');
    Route::post('save-contact', 'HomeController@saveContact')->name('save-contact');
});
Route::view('/404', 'errors.404');
Route::view('/500', 'errors.500');
Route::view('/503', 'errors.503');
