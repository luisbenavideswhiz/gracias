<?php
Route::namespace('Admin')->name('admin.')->group(function () {
//General
    Route::get('home', 'SettingController@home')->name('home.index');
    Route::get('agency', 'SettingController@agency')->name('agency.index');
    Route::post('store', 'SettingController@store')->name('setting.store');
    Route::get('contact', 'SettingController@contact')->name('contact.index');
//Category
    Route::resource('category', 'CategoryController');
//Message
    Route::resource('message', 'MessageController');
//Gallery
    Route::post('gallery/order', 'GalleryController@order');
    Route::post('gallery/{project}/store', 'GalleryController@store')->name('gallery.store');
    Route::get('gallery/{project}/create', 'GalleryController@create')->name('gallery.create');
    Route::delete('gallery/{id}/delete', 'GalleryController@destroy')->name('gallery.destroy');
//Project
    Route::resource('project', 'ProjectController');
//Client
    Route::post('client/order-client', 'ClientController@order');
    Route::resource('client', 'ClientController');
//Logs
    //Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

//Auth
/*Route::namespace('Auth')->group(function () {
    Route::get('login', 'LoginController@showLoginForm');
    Route::post('login', 'LoginController@login')->name('login');
    Route::post('logout', 'LoginController@logout')->name('logout');
});*/
Auth::routes();


